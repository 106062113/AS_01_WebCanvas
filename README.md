# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

* Brush: Click on the brush icon on the control panel to enable brush, click and hold your mouse and then drag on the canvas to draw whatever you like with brush.


![](brush.png)
* Eraser: Click on the eraser icon on the control panel to enable eraser, click and hold your mouse and move your mouse to erase the image you would like to get rid of on the canvas. The eraser will erase **anything**.


![](eraser.png)

* Crop: click on the crop button to enable crop, click and drag on the canvas the encircle the part of canvas you want. Let go the mouse and it will crop of the other part **and add a triangle around your part**


![](crop.png)
* ChangeColor: Click on the color box next to the label brushColor and a pop up window will appear. Select the color you like and that color will be apply to brush, rectangles, circles, triangles and text. If you change the color while entering text, you will need to click on the canvas to start entering again.


![](changeColr.png)
* ChangeSize: Click on the meter next to the label brushSize to adjust brush's size. This size on the meter will be applied to brush, rectangles, circles, triangles and text.


![](changeBrushSize.png)
* rectangles triangles, circle: Click on the button that has a rectangle/triangle/circle on it to enable. To draw, click and hold your mouse and move around to adjust the size and shape of your rectangle. Once you are done adjusting, let go your mouse and the rectangle will be drawn on your canvas.


![](rect.png)
![](tri.png)
![](circle.png)
* Clear: Click on the clear button to clear your canvas, but be careful **once you clear your canvas, you will not be able to get the content back using undo**.


![](clear.png)
* Undo: Click on the undo button to remove the last change on your canvas **undo button will undo the text you enter on the canvas letter by letter**.


![](undo.png)
* Redo: Click on the redo button to recover content before redo.


![](redo.png)
* Text: Click on the text button to enable text function. To enter text, first click on where you want to put your text, then start entering text using keyboard. When done, hit enter or click on other button to end the entering.


![](text.png)
* Download: Click on the download button to download the current content of your canvas, the downloaded file will be names "Image.png" automatically.


![](download.png)
* Upload: Click on the upload button to upload local file on to the canvas. After clicking on the button, a window will pop up and you can select the file you would like to upload.


![](upload.png)
* ChangeFont: Click on the drop down menu to select the font of your text. There're three font available: Arial, Comic, and Times New Roman. **Text entered previously will not be affect if you change the font**. If you change the font while entering text, you will need to click on the canvas to start entering again.


![](changeFont.png)
* ChangeFontSize: Click on the drop down menu to select the size of your text, There're also three sizes available: 30, 20, and 10. **Text entered previously will not be affect if you change the size**. If you change the size while entering text, you will need to click on the canvas to start entering again.


![](changeSize.png)
