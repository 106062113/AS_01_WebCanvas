var dragging;
var lastX ,lastY;
var startingX , startingY;
var canvas = document.querySelector('#WebCanvas');
var ctx = canvas.getContext("2d");
var color = document.getElementById("brushColor").value;
var size = document.getElementById("brushSize").valueAsNumber;
var mouseFunction = "brush";
var canvasHistory = new Array();
var historyCount = -1;
var entering = false;
var inputText = "";
var fontType = "Comic Sans MS";
var fontSize = "30px";
var fontAndSize = "30px Comic Sans MS";
var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);
canvas.style.cursor = "url('brush.png'), default";
canvas.addEventListener("mousedown", toggleMouseDown);
canvas.addEventListener("mousemove", toggleMouseMove);
canvas.addEventListener("mouseup", toggleMouseUp);
document.addEventListener("keydown", toggleKeyDown);
function pushHistory(){
    historyCount ++;
    console.log(canvasHistory);
    if (historyCount < canvasHistory.length){ canvasHistory.length = historyCount; }
    canvasHistory.push(document.getElementById('WebCanvas').toDataURL());
}

function undo() {
    console.log("undo");
    if (historyCount > 0) {
        historyCount--;
        var canvasPic = new Image();
        canvasPic.src = canvasHistory[historyCount];
        //clearCanvas();
        if ((mouseFunction === "rect" || mouseFunction === "tri" || mouseFunction === "circle") && dragging){
            ctx.drawImage(canvasPic, 0, 0);
        }
        else{
            canvasPic.onload = function (){ctx.drawImage(canvasPic, 0, 0)};
        }

    }
    else if (historyCount === 0){
        historyCount--;
        clearCanvas();
    }
}

function redo() {
    if (historyCount < canvasHistory.length - 1){
        historyCount ++;
        var canvasPic = new Image();
        canvasPic.src = canvasHistory[historyCount];
        canvasPic.onload = function () {ctx.drawImage(canvasPic, 0 , 0 )};
    }
}
function changeToBrush(){
    mouseFunction = "brush";
    entering = false;
    canvas.style.cursor = "url('brush.png'), default" ;
    console.log("brush");
    inputText = "";
}

function changeToEraser(){
    mouseFunction = "eraser";
    entering = false;
    canvas.style.cursor = "url('eraser.png'), default" ;
    inputText = "";
}

function changeToText(){
    mouseFunction = "text";
    canvas.style.cursor = "text";
}
function changeToRect(){
    mouseFunction = "rect";
    canvas.style.cursor = "crosshair";
}

function changeToTri(){
    mouseFunction = "tri";
    canvas.style.cursor = "crosshair";
}

function changeToCircle(){
    mouseFunction = "circle";
    canvas.style.cursor = "crosshair";
}

function changeToCrop(){
    mouseFunction = "crop";
    canvas.style.cursor = "crosshair";
}
function clearCanvas(){
    historyCount = -1;
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function downloadCanvas(){
    image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "Image.png";
    link.href = image;
    link.click();
}
function changeColor(){
    color = document.getElementById("brushColor").value;
    entering = false;
    inputText = "";
}
function changeBrushSize(){
    //console.log("changed");
    size = document.getElementById("brushSize").valueAsNumber;
    //console.log(size)
}

function changeFont(){
    fontType = document.getElementById("fontSelect").value;
    fontAndSize = fontSize + " " + fontType;
    entering = false;
    inputText = "";
}

function changeSize(){
    fontSize = document.getElementById("sizeSelect").value;
    fontAndSize = fontSize + " " + fontType;
    entering = false;
    inputText = ""
}
function toggleMouseUp(evt){
    if (dragging){
        if (mouseFunction === "rect" || mouseFunction === "tri" || mouseFunction === "circle"){
            historyCount --;
        }
        if (mouseFunction === "crop"){
            historyCount = historyCount - 2;
            dragging = false;
            crop()
        }
        pushHistory();
    }
    dragging = false;
}
function toggleMouseDown(evt){
    if (mouseFunction === "brush" || mouseFunction === "eraser"){
        dragging = true;
    }
    else if (mouseFunction === "rect" || mouseFunction === "tri" || mouseFunction === "circle" || mouseFunction === "crop"){
        pushHistory();
        dragging = true;
    }
    else if (mouseFunction === "text" && entering){
        changeToBrush();
        entering = false;
        inputText = "";
    }
    else if (mouseFunction === "text" && !entering){
        entering = true;
        pushHistory();
    }
    //console.log("done");
    var mousePos = getMousePos(canvas, evt);
    lastX = mousePos.x;
    lastY = mousePos.y;
    startingX = mousePos.x;
    startingY = mousePos.y;
}
function toggleMouseMove(evt){
    if (mouseFunction === "brush"){
        draw(evt);
    }
    else if (mouseFunction === "eraser"){
        erase(evt);
    }
    else if (mouseFunction === "rect" || mouseFunction === "crop"){
        Drawrect(evt);
    }
    else if (mouseFunction === "tri"){
        Drawtri(evt);
    }
    else if (mouseFunction === "circle"){
        DrawCircle(evt);
    }
}

function toggleKeyDown(evt){
    //console.log("enter");
    if (mouseFunction === "text" && entering){
        if (evt.keyCode === 8){
            if (inputText.length >= 1){
                console.log("remove");
                console.log(inputText);
                inputText = inputText.slice(0, inputText.length - 1);
                ctx.font = fontAndSize;
                ctx.fillStyle = "red";
                undo();
                //ctx.fillText(inputText, lastX, lastY);
            }
        }
        else if(evt.keyCode === 13){
            changeToBrush();
            inputText = "";
        }
        else{
            inputText = inputText + String.fromCharCode(evt.keyCode);
            ctx.font = fontAndSize;
            console.log(fontAndSize);
            ctx.fillStyle = color;
            ctx.fillText(inputText, lastX, lastY);
            pushHistory();
        }

    }
}

function draw(evt){
    //console.log("draw");
    if (dragging){
        ctx.lineCap = "round";
        ctx.lineWidth = size;
        ctx.strokeStyle = color;
        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        ctx.closePath();
        lastX = mousePos.x;
        lastY = mousePos.y;

    }
}

function erase(evt){
    if (dragging){
        ctx.lineCap = "round";
        ctx.lineWidth = size;
        ctx.strokeStyle = "white"
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        ctx.closePath();
        lastX = mousePos.x;
        lastY = mousePos.y;

    }
}

function Drawrect(evt){
    if (dragging){
        undo();
        if (mouseFunction === "rect"){
            ctx.strokeStyle = color;
            ctx.lineCap = "round";
            ctx.lineWidth = size;
        }
        else{
            ctx.strokeStyle = "gray";
            ctx.lineCap = "round";
            ctx.lineWidth = 2;
        }
        var mousePos = getMousePos(canvas, evt);
        ctx.beginPath()
        let posx = startingX;
        let posy = startingY;
        if (mousePos.x < startingX){
            posx = mousePos.x;
        }
        if (mousePos.y < startingY){
            posy = mousePos.y;
        }
        ctx.rect(posx, posy, Math.abs(startingX - mousePos.x), Math.abs(startingY - mousePos.y));
        console.log(startingX, startingY, Math.abs(startingX - mousePos.x), Math.abs(startingY - mousePos.y));
        ctx.stroke();
        ctx.closePath();
        lastX = mousePos.x;
        lastY = mousePos.y;
        pushHistory();
    }
}

function Drawtri(evt){
    if (dragging){
        undo();
        var mousePos = getMousePos(canvas, evt);
        ctx.beginPath()
        ctx.strokeStyle = color;
        ctx.lineCap = "round";
        ctx.lineWidth = size;
        ctx.moveTo(startingX, startingY);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.lineTo(startingX, mousePos.y);
        ctx.lineTo(startingX, startingY);
        ctx.stroke();
        ctx.closePath();
        pushHistory();
    }
}

function DrawCircle(evt){
    if (dragging){
        undo();
        var mousePos = getMousePos(canvas, evt);
        ctx.beginPath()
        ctx.strokeStyle = color;
        ctx.lineCap = "round";
        ctx.lineWidth = size;
        ctx.arc(startingX, startingY, Math.sqrt((startingX - mousePos.x) * (startingX - mousePos.x) + (startingY - mousePos.y) * (startingY - mousePos.y)), 0, 2 * Math.PI);
        ctx.stroke();
        ctx.closePath();
        pushHistory();
    }
}
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: (evt.clientX - rect.left),
        y: (evt.clientY - rect.top)
    };
}

function crop(){
    ctx.fillStyle = "white";
    if (lastY < startingY){
        ctx.fillRect(0, 0, canvas.width, lastY);
        ctx.fillRect(0, startingY, canvas.width, canvas.height - startingY);
    }
    else{
        ctx.fillRect(0, 0, canvas.width, startingY);
        ctx.fillRect(0, lastY, canvas.width, canvas.height - lastY);
    }
    if (lastX < startingX){
        ctx.fillRect(0, 0, lastX, canvas.height);
        ctx.fillRect(canvas.width - startingX, 0, canvas.width - startingX, canvas.height);
    }
    else{
        ctx.fillRect(0, 0, startingX, canvas.height);
        ctx.fillRect(lastX, 0, canvas.width - lastX, canvas.height);
    }
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.globalCompositeOperation = "source-over";
            ctx.drawImage(img,0,0);
            pushHistory();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}
